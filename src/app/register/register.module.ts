import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { registerPageRoutingModule } from './register-routing.module';
import { registerPage } from './register.page';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    registerPageRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  declarations: [registerPage]
})
export class registerPageModule {}
