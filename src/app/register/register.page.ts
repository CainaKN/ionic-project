import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { CategoriesService } from '../categories/categories.service';
import { HttpClient } from '@angular/common/http';

declare let paypal;

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class registerPage implements OnInit {
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;

  product = {
    price: 80.00,
    description: 'Pagamento de Assinatura',
  };

  constructor(private router: Router, private formBuilder: FormBuilder, private appService: CategoriesService,
    private httpClient: HttpClient) 
  {}
  
  loginForm: FormGroup;
  categorys = [];

  getCategory() {
    return this.appService.getCategories();
  }

  // createUser() {
  //   this.httpClient.post('http://localhost:3000/user/create', this.loginForm.value).subscribe(a => a);
  // }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: this.formBuilder.control('',  [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      password: this.formBuilder.control('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      endereco: this.formBuilder.control('', Validators.required),
      name: this.formBuilder.control('', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      cpfOrCnpj: this.formBuilder.control('', Validators.required),
      category: this.formBuilder.control([''])
    })

    of(this.getCategory()).subscribe(cat => {
      this.categorys = cat;
      this.loginForm.controls.category.patchValue(this.categorys[0].id);
    });

    paypal.Buttons({
      style: {
        tagline: false,
        color: 'silver',
        // layout: 'horizontal',
        shape: 'pill',
        size: 'responsive',
      },
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: this.product.description,
                amount: {
                  currency_code: 'BRL',
                  value: this.product.price
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          // this.createUser();
          console.log('order', order);
          if(order.status.toLowerCase() === 'completed'){
            console.log("AEEEE DEU CERTO");
          }
          // this.router.navigateByUrl('/login')
        },
        onError: err => {
          console.log(err);
        }
      })
      .render(this.paypalElement.nativeElement);
  }
}

