import { CategoriesService } from './categories.service';
import { Categories } from './categories.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  providers: [CategoriesService],
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {

  constructor(private catService: CategoriesService) { }

  categories: Categories[];

  ngOnInit() {
    this.categories = this.catService.getCategories();
  }

}
