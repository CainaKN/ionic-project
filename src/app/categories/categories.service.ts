import { DetailModel } from './../categories-detail/detail.model';
import { HttpClient } from '@angular/common/http';
import { Categories } from './categories.model';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CategoriesService {
    constructor(private http: HttpClient){}

    categories: Categories[] = [
        {image: '../../assets/img/categories/alimentacao.png', name: 'Aliementação', id: 'alimentacao'},
        {image: '../../assets/img/categories/academias.png', name: 'Academias', id: 'academias'},
        {image: '../../assets/img/categories/automotivo.png', name: 'Automotivo', id: 'automotivo'},
        {image: '../../assets/img/categories/assistencia.png', name: 'Assistência', id: 'assistencia'},
        {image: '../../assets/img/categories/pet.png', name: 'Pet', id: 'pet'},
        {image: '../../assets/img/categories/moda.png', name: 'Moda e Beleza', id: 'moda'},
        {image: '../../assets/img/categories/educacao.png', name: 'Educação', id: 'educacao'},
        {image: '../../assets/img/categories/saude.png', name: 'Saúde', id: 'saude'},
        {image: '../../assets/img/categories/geral.png', name: 'Serviços Gerais', id: 'geral'}
    ]

    getCategories(): Array<Categories>{
        return this.categories;
    }

    getByID(id: string): Observable<DetailModel[]> {
        const APIurl = 'http://localhost:3000/user';
        const url = `${APIurl}/${id}`
        return this.http.get<DetailModel[]>(url);
    }
}