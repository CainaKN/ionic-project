export class Categories {
    constructor(public image: string, public name: string, public id: string){
        this.image = image;
        this.name = name;
        this.id = id;
    }
}