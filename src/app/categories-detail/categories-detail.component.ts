import { DetailModel } from './detail.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { CategoriesService } from '../categories/categories.service';

@Component({
  selector: 'app-categories-detail',
  templateUrl: './categories-detail.component.html',
  styleUrls: ['./categories-detail.component.scss'],
})
export class CategoriesDetailComponent implements OnInit {
  @ViewChild(IonContent, { static: true}) content: IonContent;

  itens: Array<DetailModel> = [];
  searchText: string = null;
  
  constructor(private service: CategoriesService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.service.getByID(this.route.snapshot.params.id).subscribe((data: DetailModel[]) => {
      this.itens = data;
    });
  }

  scrollContent(scroll) {
    scroll == 'top' ? this.content.scrollToTop(300) : this.content.scrollToBottom(300);
  }

  openWhatsApp(): void {
    window.open('https://api.whatsapp.com/send?phone=11977880438');
  }
}
