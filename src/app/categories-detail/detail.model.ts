export interface DetailModel {
    password: string;
    name: string;
    endereco: string;
    email: string;
    category: string;
    cpfOrCnpj: string
}